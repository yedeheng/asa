## user guide for ASA
There are two versions, one invokes gappa system call inside ASA, the other builds error/area models inside ASA. 
- ./src\_\* contains modified ASA source code. 
- ./diode contains scripts and related vivado code for diode.tdfc

- There are two versions of gappa script, one for gappa old version 0.16, another for new version 1.0. 

- To use this ASA package,
    1. cp all the contents under dir diode into /tmp. 
    2. cd ./src and type in "make" (make sure you call the right gappa script in asa\_usr\_cst.c file. )
