// tdfc-autoesl backend autocompiled body file
// tdfc version 1.160
// Mon Oct 28 10:36:36 2013

#include "math.h"
#include "diode.h"

void diode(
	data_t1 cc_v,
	data_t8 *cc_i
)
{
    data_t2 cc_vj = (0.0258);
    data_t3 cc_isat = (0.001000);
{ 
	*cc_i = (data_t8)((data_t7)(cc_isat*(data_t6)((data_t5)(exp_flopoco((data_t4)(cc_v/cc_vj)))-(1))));
}
}
data_t5 exp_flopoco(data_t4 in)
{
	return in*in;
}
