// tdfc-autoesl autocompiled wrapper file
// tdfc version 1.160
// Mon Oct 28 10:36:36 2013

#include <stdio.h>
#include <math.h>
#include "diode.h"

int main()
{
  int N=1024;
  const double cc_vj = (double)1;
  const double cc_isat = (double)1;
  double *cc_v;
  double *cc_i;

  // cc_v
  size_t cc_v_size = N * sizeof(double);
  cc_v = (double *)malloc(cc_v_size);

  // cc_i
  size_t cc_i_size = N * sizeof(double);
  cc_i = (double *)malloc(cc_i_size);

  // Initialize input contents and copy to Device memory
  for(int i=0; i<N; i++) {
    cc_v[i] = (double)i;
  }

  // Loop
  for(int i=0; i<N; i++) {
    diode (
      cc_vj,
      cc_isat,
      cc_v[i],
      &cc_i[i]
    );
  }

  // Print results (typecasted to prevent printf errors)
  printf("diode:\n");
  for (int i=0; i<N; i++) printf("cc_i[%d] = %g\n", i, cc_i[i]);

  // Cleanup
  free(cc_v); 
  free(cc_i); 

} // main
