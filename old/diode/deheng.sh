#!/bin/zsh

cd /tmp
vivado_hls -f diode.tcl &> /dev/null
cat /tmp/diode_batch.prj/solution1/syn/report/diode_csynth.rpt | grep "Utilization Estimates" -A 14 | tail -n 1 | cut -d"|" -f 6 | sed "s/\s//g"
#var=`cat diode_batch.prj/solution1/syn/report/diode_csynth.rpt | grep "Utilization Estimates" -A 14 | tail -n 1 | cut -d"|" -f 6 | sed "s/\s//g"`
#echo "$var" > cost.csv
