# tdfc-autoesl autocompiled wrapper file
# tdfc version 1.160
# Mon Oct 28 10:36:36 2013

set TOP [file rootname [info script]]
delete_project	${TOP}_batch.prj
open_project	${TOP}_batch.prj
add_files 		${TOP}.cpp
set_top  		${TOP}
open_solution	solution1
set_part 		xc7k160tfbg484-2
create_clock	-period 3ns
set_directive_inline -off exp_flopoco
csynth_design
export_design
exit
