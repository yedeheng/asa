# tdfc-gappa backend autocompiled body file
# tdfc version 1.160
# Tue Oct 15 13:44:02 2013


@fx1 = fixed<-56,ne>;
@fx2 = fixed<-61,ne>;
@fx3 = fixed<-70,ne>;
@fx4 = fixed<-47,ne>;
@fx5 = fixed<-70,ne>;
@fx6 = fixed<-49,ne>;
@fx7 = fixed<-55,ne>;
@fx8 = fixed<-54,ne>;

vj_m = float<ieee_64,ne>(0.0258);
isat_m = float<ieee_64,ne>(0.001000);
v_m = float<ieee_64,ne>(v);
divide_7_m  = (v_m / vj_m);
exp_9_m  = exp ( divide_7_m );
minus_10_m  = (exp_9_m - 1);
multiply_11_m  = (isat_m * minus_10_m);
i_m  =  ( multiply_11_m );

vj_dbl = float<ieee_64,ne>(0.0258);
isat_dbl = float<ieee_64,ne>(0.001000);
v_dbl = float<ieee_64,ne>(v);
divide_7_dbl float<ieee_64,ne> = (v_dbl / vj_dbl);
exp_9_dbl float<ieee_64,ne> = exp ( divide_7_dbl );
minus_10_dbl float<ieee_64,ne> = (exp_9_dbl - 1);
multiply_11_dbl float<ieee_64,ne> = (isat_dbl * minus_10_dbl);
i_dbl float<ieee_64,ne> =  ( multiply_11_dbl );

vj_fx = fx1(0.0258);
isat_fx = fx2(0.001000);
v_fx = fx3(v);
divide_7_fx fx4 = (v_fx / vj_fx);
exp_9_fx fx5 = exp ( divide_7_fx );
minus_10_fx fx6 = (exp_9_fx - 1);
multiply_11_fx fx7 = (isat_fx * minus_10_fx);
i_fx fx8 =  ( multiply_11_fx );


{

		v in [1e-06,0.1] /\ 
		
		(i_m >= 0x1p-53 \/ i_m <= - 0x1p-53)

		->
		(i_dbl-i_m)/i_m in ? /\ 
		(i_fx-i_m)/i_m in ? /\ 
		(i_m-i_fx) in ? /\ 
		(i_m-i_dbl) in ?
}

#diode
