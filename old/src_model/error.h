#ifndef AREA_H_
#define AREA_H
#include<assert.h>
#include<math.h>
#include "opcode.h"
#define REGISTERS 64
#include<iostream>
#include<vector>
void exp_errrule(double x0, double x1, double e1,
	double t,
	double* rete) {
    assert(x1>=x0);
    double e3 = pow((double)2,(double)-(t+1)); 
    *rete = exp(x1) * ( exp(e1) - 1 ) + e3;
}
void log_errrule(double x0, double x1, double e1,
	double t,
	double* rete) {
    assert(x0>0 && x1>0 && x1>x0);
    double e3 = pow((double)2,(double)-(t+1)); 
    *rete = log(1 + e1/x0) + e3;
}

void div_errrule(double x0, double x1, double e1,
	double y0, double y1, double e2,
	double t,
	double* rete) {
    // error equation from Haldar paper
    // add re-writing rule 
    double e3 = pow((double)2,(double)-(t+1)); 
    double rep_x = ( fabs(x1)>fabs(x0) )?fabs(x1):fabs(x0); 
    double rep_y = ( fabs(y1)>fabs(y0) )?fabs(y1):fabs(y0); 
    double rep_y_min = ( fabs(y1)>fabs(y0) )?fabs(y0):fabs(y1); 
    *rete = e3 + (rep_y*e1 + rep_x*e2) / ( rep_y*e2 + rep_y*rep_y );
    //*rete = (e1 - e2*rep_x/rep_y) / rep_y;
}

void mult_errrule(double x0, double x1, double e1,
	double y0, double y1, double e2,
	double t,
	double* rete) {
    // error equation from Haldar paper
    //*rete = ((x1+x0)/2)*e2 + ((y1+y0)/2)*e1 + pow((double)2,(double)-(t+1));
    double e3 = pow((double)2,(double)-(t+1)); 
    double main_err = ( ( fabs(x1)>fabs(x0) )?fabs(x1):fabs(x0) )*e2 + ( ( fabs(y1)>fabs(y0) )?fabs(y1):fabs(y0) )*e1; 
    //double minor_err = (e1*e2)?pow((double)2,(double)-(t+1)):0;
    if (e1==0 || e2==0)
	*rete = main_err;
    else 
	*rete = main_err + e3 + e1*e2;
}

void add_errrule(double x0, double x1, double e1,
	double y0, double y1, double e2,
	double t,
	double* rete) {
    double err_cur = pow((double)2,(double)-(t+1)); // error for current instruction
    double max_e1_e2 = (e1>=e2)?e1:e2;
    if (err_cur > max_e1_e2)
	*rete = err_cur + e1 + e2;
    else 
	*rete = e2 + e1; // + 2**(-t);
}

void sub_errrule(double x0, double x1, double e1,
	double y0, double y1, double e2,
	double t,
	double *rete) {
    // error equation from Haldar paper
    double err_cur = pow((double)2,(double)-(t+1)); // error for current instruction
    double max_e1_e2 = (e1>=e2)?e1:e2;
    if (err_cur > max_e1_e2)
	*rete = err_cur + e1 + e2;
    else 
	*rete = e2 + e1; // + 2**(-t);
}

double peace_error(std::vector<double> temp_lo, std::vector<double> temp_hi, 
	std::vector<int> opcode, std::vector<int> src0, std::vector<int> src1, std::vector<int> dest, int INSTRUCTIONS, double* x)
{
    double out_err; 
    double error[REGISTERS];
    int t0;

    for(int j=0;j<INSTRUCTIONS;j++) 
    {
	t0 = (int)x[j];

	if(opcode[j]==LOG) {
	    log_errrule(temp_lo[src0[j]], temp_hi[src0[j]], error[src0[j]],
		    //temp_lo[src1[j]], temp_hi[src1[j]], error[src1[j]], 
		    t0, &error[dest[j]]);
	} else if(opcode[j]==EXP) {
	    exp_errrule(temp_lo[src0[j]], temp_hi[src0[j]], error[src0[j]],
		    //temp_lo[src1[j]], temp_hi[src1[j]], error[src1[j]], 
		    t0, &error[dest[j]]);
	} else if(opcode[j]==DIV) {
	    div_errrule(temp_lo[src0[j]], temp_hi[src0[j]], error[src0[j]],
		    temp_lo[src1[j]], temp_hi[src1[j]], error[src1[j]], 
		    t0, &error[dest[j]]);
	} else if(opcode[j]==MUL) {
	    mult_errrule(temp_lo[src0[j]], temp_hi[src0[j]], error[src0[j]],
		    temp_lo[src1[j]], temp_hi[src1[j]], error[src1[j]], 
		    t0, &error[dest[j]]);
	} else if(opcode[j]==ADD) {
	    add_errrule(temp_lo[src0[j]], temp_hi[src0[j]], error[src0[j]],
		    temp_lo[src1[j]], temp_hi[src1[j]], error[src1[j]], 
		    t0, &error[dest[j]]);
	} else if(opcode[j]==SUB) {
	    sub_errrule(temp_lo[src0[j]], temp_hi[src0[j]], error[src0[j]],
		    temp_lo[src1[j]], temp_hi[src1[j]], error[src1[j]], 
		    t0, &error[dest[j]]);
	} else if(opcode[j]==LD) {
	    double in_hi_temp=temp_hi[dest[j]];
	    double in_lo_temp=temp_lo[dest[j]];
	    if(in_hi_temp==in_lo_temp) {
		double shift = in_lo_temp*pow((double)2,(double)t0);
		double intpart;
		double fractpart = modf(shift, &intpart);
		if(fractpart)
		{
		    error[dest[j]]=fractpart * pow((double)2,(double)-(t0+1));
		}
		else 
		    error[dest[j]]=0;
		} else {
		    error[dest[j]]=pow((double)2,(double)-(t0+1));
		}
	} else if(opcode[j]==ST) {
	    out_err = error[src0[j]];
	}
    }
    return out_err;
} 



#endif
