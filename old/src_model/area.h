#ifndef AREA_H_
#define AREA_H

#define REGISTERS 64
#include<vector>

double exp_area(int x) {
    double area = 2718.857 + 18.66216*x + 0.05235931*x*x - 4.570707e-04*x*x*x;
    return area;
}

double log_area(int x) {
    double area = 2032.929 + 19.08337*x + 0.05482684*x*x - 5.176768e-04*x*x*x;
    return area;
}

double div_area(int x) {
    double area = 252.1429 - 84.09784*x + 8.084491*x*x + 2.272727e-05*x*x*x;
    return area;
}

double mult_area(int x) {
    double area = x*x;
    return area;
}

double sub_area(int x) {
    double area = x + 1;
    return area;
}

double add_area(int x) {
    double area = x;
    return area;
}

double peace_area(std::vector<int> opcode, std::vector<int> dest, int INSTRUCTIONS, double* x)
{
    double out_area;
    double area[REGISTERS];
    double area_total = 0;
    int t0;

    for(int j=0;j<INSTRUCTIONS;j++) 
    {
	t0= (int)x[j];
	if(opcode[j]==LOG) {
	    area_total += log_area(t0);
	} else if(opcode[j]==EXP) {
	    area_total += exp_area(t0);
	} else if(opcode[j]==DIV) {
	    area_total += div_area(t0);
	} else if(opcode[j]==MUL) {
	    area_total += mult_area(t0);
	} else if(opcode[j]==ADD) {
	    area_total += add_area(t0);
	} else if(opcode[j]==SUB) {
	    area_total += sub_area(t0);
	} else if(opcode[j]==LD) {
	    continue;
	} else if(opcode[j]==ST) {
	    //printf("area : %g \n", area_total);
	    out_area = area_total;
	    area_total = 0;
	}
    }
    return out_area;
} 

#endif
