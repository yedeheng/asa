#ifndef RANGE_H_
#define RANGE_H_

double max(double t1, double t2)
{
    return (t1>t2)?t1:t2;
}

double min(double t1, double t2)
{
    return (t1>t2)?t2:t1;
}

void log_rule(double x0, double x1, 
        double* ret0, double* ret1) {
    if(x0<=0 || x1<=0)
        return;
    *ret1= log(x1);
    *ret0= log(x0);
}
void exp_rule(double x0, double x1, 
        double* ret0, double* ret1) {
    *ret1= exp(x1);
    *ret0= exp(x0);
}

void div_rule(double x0, double x1, 
        double y0, double y1, 
        double* ret0, double* ret1) {
    double t1 = x1/y1;
    double t2 = x1/y0;
    double t3 = x0/y1;
    double t4 = x0/y0;
    *ret1=max(t1,max(t2,max(t3,t4)));
    *ret0=min(t1,min(t2,min(t3,t4)));
}

void mult_rule(double x0, double x1, 
        double y0, double y1, 
        double* ret0, double* ret1) {
    double t1 = x1*y1;
    double t2 = x1*y0;
    double t3 = x0*y1;
    double t4 = x0*y0;

    *ret1=max(t1,max(t2,max(t3,t4)));
    *ret0=min(t1,min(t2,min(t3,t4)));
}
void add_rule(double x0, double x1, 
        double y0, double y1, 
        double* ret0, double* ret1) {
    double t1 = x1+y1;
    double t2 = x0+y0;

    *ret1=max(t1,t2);
    *ret0=min(t1,t2);
}

void sub_rule(double x0, double x1, 
        double y0, double y1, 
        double* ret0, double* ret1) {
    double t1 = x1-y0;
    double t2 = x0-y1;

    *ret1=max(t1,t2);
    *ret0=min(t1,t2);
}

#endif
