#ifndef AREA_H_
#define AREA_H_

/* standard headers */
#include<vector>
#include<math.h>

/* self-defined headers */
#include "opcode.h"
#include "range.h"

double exp_area(int x) {
    double area = 2718.857 + 18.66216*x + 0.05235931*x*x - 4.570707e-04*x*x*x;
    return area;
}

double log_area(int x) {
    double area = 2032.929 + 19.08337*x + 0.05482684*x*x - 5.176768e-04*x*x*x;
    return area;
}

double div_area(int x) {
    double area = 252.1429 - 84.09784*x + 8.084491*x*x + 2.272727e-05*x*x*x;
    return area;
}

double mult_area(int x1, int x2) {
    double area = x1*x2;
    return area;
}

double sub_area(int x1, int x2) {
    double area = (x1>x2)?x1:x2 + 1;
    return area;
}

double add_area(int x1, int x2) {
    double area = (x1>x2)?x1:x2;
    return area;
}


int integer_bit_calc(const double& low_bound, const double& high_bound)
{
    int lo = (int)low_bound;
    int hi = (int)high_bound;
    int max_abs = (abs(lo)>abs(hi))?abs(lo):abs(hi);
    int integer_bit = ceil( log2(max_abs+1) );
    return integer_bit;
}

double peace_area(std::vector<double> temp_lo, std::vector<double> temp_hi, std::vector<int> opcode, std::vector<int> dest, std::vector<int> src0,std::vector<int> src1, int INSTRUCTIONS, double* x)
{
    double out_area;
    int bits[REGISTERS]={0};
    double area[REGISTERS]={0};
    double area_total = 0;
    int t0;

    int integer_bit_src0;
    int integer_bit_src1;

    for(int j=0;j<INSTRUCTIONS;j++) 
    {
        t0= (int)x[j];
        if(opcode[j]==LOG)
        {
            log_rule(temp_lo[src0[j]], temp_hi[src0[j]], &temp_lo[dest[j]], &temp_hi[dest[j]]);
            integer_bit_src0 = integer_bit_calc(temp_lo[src0[j]], temp_hi[src0[j]]);
            bits[dest[j]] = t0;
            area_total += log_area(integer_bit_src0 + bits[src0[j]]);
        } 
        else if(opcode[j]==EXP) 
        {
            exp_rule(temp_lo[src0[j]], temp_hi[src0[j]], &temp_lo[dest[j]], &temp_hi[dest[j]]);
            integer_bit_src0 = integer_bit_calc(temp_lo[src0[j]], temp_hi[src0[j]]);
            bits[dest[j]] = t0;
            area_total += exp_area(integer_bit_src0 + bits[src0[j]]);
        }
        else if(opcode[j]==DIV) 
        {
            div_rule(temp_lo[src0[j]], temp_hi[src0[j]], temp_lo[src1[j]], temp_hi[src1[j]], &temp_lo[dest[j]], &temp_hi[dest[j]]);
            integer_bit_src0 = integer_bit_calc(temp_lo[src0[j]], temp_hi[src1[j]]);
            integer_bit_src1 = integer_bit_calc(temp_lo[src1[j]], temp_hi[src1[j]]);
            bits[dest[j]] = t0;
            area_total += div_area(0); // Currently no div is used. 
        }
        else if(opcode[j]==MUL) 
        {
            mult_rule(temp_lo[src0[j]], temp_hi[src0[j]], temp_lo[src1[j]], temp_hi[src1[j]], &temp_lo[dest[j]], &temp_hi[dest[j]]);
            integer_bit_src0 = integer_bit_calc(temp_lo[src0[j]], temp_hi[src1[j]]);
            integer_bit_src1 = integer_bit_calc(temp_lo[src1[j]], temp_hi[src1[j]]);
            bits[dest[j]] = t0;
            area_total += mult_area(integer_bit_src0 + bits[src0[j]], integer_bit_src1 + bits[src1[j]]);
        }
        else if(opcode[j]==ADD)
        {
            add_rule(temp_lo[src0[j]], temp_hi[src0[j]], temp_lo[src1[j]], temp_hi[src1[j]], &temp_lo[dest[j]], &temp_hi[dest[j]]);
            int integer_bit_src0 = integer_bit_calc(temp_lo[src0[j]], temp_hi[src1[j]]);
            int integer_bit_src1 = integer_bit_calc(temp_lo[src1[j]], temp_hi[src1[j]]);
            bits[dest[j]] = t0;
            area_total += add_area(integer_bit_src0 + bits[src0[j]], integer_bit_src1 + bits[src1[j]]);
        }
        else if(opcode[j]==SUB) 
        {
            sub_rule(temp_lo[src0[j]], temp_hi[src0[j]], temp_lo[src1[j]], temp_hi[src1[j]],&temp_lo[dest[j]], &temp_hi[dest[j]]);
            integer_bit_src0 = integer_bit_calc(temp_lo[src0[j]], temp_hi[src1[j]]);
            integer_bit_src1 = integer_bit_calc(temp_lo[src1[j]], temp_hi[src1[j]]);
            bits[dest[j]] = t0;
            area_total += sub_area(integer_bit_src0 + bits[src0[j]], integer_bit_src1 + bits[src1[j]]);
        } 
        else if(opcode[j]==LD) 
        {
            bits[dest[j]] = t0;
        }
        else if(opcode[j]==ST) 
        {
            area_total += bits[src0[j]]; // minimize ST bits
        }
    }
    out_area = area_total;
    area_total = 0;
    return out_area;
} 

#endif
