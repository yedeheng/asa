#include <stdio.h>
#include <stdlib.h>
int main (int argc, char* argv[])
{
    FILE* fp = fopen(argv[1], "r");
    char buf[256];
    int min = 10000;
    int line_cnt= 0;
    double runtime;
    while (fgets (buf, sizeof(buf), fp)) {
        printf("integer: %d  ", atoi(buf));
        printf("line: %s", buf);
        if (min > atoi(buf))
            min = atoi(buf);
        line_cnt++;
        runtime = double(45*line_cnt)/double(1226);
        printf("\n time: %f, minimum every iter is: %d\n", runtime, min);
    }
    return 0;
}
