#ifndef ERROR_H_
#define ERROR_H_

/* standard headers*/
#include<assert.h>
#include<math.h>
#include<iostream>
#include<stdio.h>
#include<vector>

/* self-defined headers */
#include "opcode.h"
#include "range.h"

void exp_errrule(double x0, double x1, double e1,
        int t,
        double* rete) {
    assert(x1>=x0);
    double e3 = pow(2,-(t+1)); 
    *rete = exp(x1) * ( exp(e1) - 1 ) + e3;
}
void log_errrule(double x0, double x1, double e1,
        int t,
        double* rete) {
    assert(x0>0 && x1>0 && x1>x0);
    double e3 = pow(2,-(t+1)); 
    *rete = log(1 + e1/x0) + e3;
}

void div_errrule(double x0, double x1, double e1,
        double y0, double y1, double e2,
        int t,
        double* rete) {
    // error equation from Haldar paper
    // add re-writing rule 
    double e3 = pow(2,-(t+1)); 
    double rep_x = ( fabs(x1)>fabs(x0) )?fabs(x1):fabs(x0); 
    double rep_y = ( fabs(y1)>fabs(y0) )?fabs(y1):fabs(y0); 
    double rep_y_min = ( fabs(y1)>fabs(y0) )?fabs(y0):fabs(y1); 
    *rete = e3 + (rep_y*e1 + rep_x*e2) / ( rep_y*e2 + rep_y*rep_y );
    //*rete = (e1 - e2*rep_x/rep_y) / rep_y;
}

void mult_errrule(double x0, double x1, double e1,
        double y0, double y1, double e2,
        int t,
        double* rete) {
    double e3 = pow(2,-(t+1)); 
    //std::cout << "x0 " << x0 << " ";
    //std::cout << "x1 " << x1 << " ";
    //std::cout << "y0 " << y0 << " ";
    //std::cout << "y1 " << y1 << " ";
    //std::cout << "e1 " << e1 << " ";
    //std::cout << "e2 " << e2 << " ";
    //std::cout << std::endl;
    double main_err = ( ( fabs(x1)>fabs(x0) )?fabs(x1):fabs(x0) )*e2 + ( ( fabs(y1)>fabs(y0) )?fabs(y1):fabs(y0) )*e1; 
    
    if( fabs(x0-x1)<1e-30 && fabs(y0-y1)<1e-30)
    {
        double z = x1*y1;
        double z_shift = z*pow(2,t);
        double intpart;
        double fractpart = modf(z_shift, &intpart);
        if(fractpart)
            *rete = main_err + e3;
        else 
            *rete = main_err;
    }
    else if ( fabs(x0-x1)<1e-30 && fabs(y0-y1)>1e-30)
    {
        double intpart;
        double fracpart = modf(x1, &intpart);
        if(fracpart) // not an integer
            *rete = main_err + e3;
        else  // it is integer
        {
            int x_int = (int)x1; 
            while (((x_int % 2) == 0) && x_int > 1) // detect pow of 2
                x_int /= 2;
            if(x_int == 1) // if it is a pow of 2
            {
                int error_ratio = e3/e2;
                if(error_ratio <= x_int)
                    *rete = main_err;
                else 
                    *rete = main_err + e3;
            }
        }
    }
    else if (fabs(x0-x1)>1e-30 && fabs(y0-y1)<1e-30)
    {
        double intpart;
        double fracpart = modf(y1, &intpart);
        if(fracpart) // not an integer
            *rete = main_err + e3;
        else  // it is integer
        {
            int y_int = (int)y1; 
            while (((y_int % 2) == 0) && y_int > 1) // detect pow of 2
                y_int /= 2;
            if(y_int == 1) // if it is a pow of 2
            {
                int error_ratio = e3/e1;
                if(error_ratio <= y_int)
                    *rete = main_err;
                else 
                    *rete = main_err + e3;
            }
        }
    }
    else
    {
        *rete = main_err + e3;
        //printf("error of multipler is : %f", *rete);
        //printf("main_error of multipler is : %f", main_err);
        //printf("e3 of multipler is : %f", e3);
    }
}

void add_errrule(double x0, double x1, double e1,
        double y0, double y1, double e2,
        int t,
        double* rete) {
    double e3 = pow(2,-(t+1)); // error for current instruction
    double max_e1_e2 = (e1>=e2)?e1:e2;
    if (e3 > max_e1_e2)
        *rete = e3 + e1 + e2;
    else 
        *rete = e2 + e1; // + 2**(-t);
    //printf("error of add is : %f", *rete);
}

void sub_errrule(double x0, double x1, double e1,
        double y0, double y1, double e2,
	int t,
	double *rete) {
    // error equation from Haldar paper
    double e3 = pow(2,-(t+1)); // error for current instruction
    double max_e1_e2 = (e1>=e2)?e1:e2;
    if (e3 > max_e1_e2)
	*rete = e3 + e1 + e2;
    else 
	*rete = e2 + e1; // + 2**(-t);
    //printf("error of sub is : %f", *rete);
}

std::vector<double> peace_error(std::vector<double> temp_lo, std::vector<double> temp_hi, 
	std::vector<int> opcode, std::vector<int> src0, std::vector<int> src1, std::vector<int> dest, int INSTRUCTIONS, double* x)
{
    std::vector<double> out_err;
    
    int bits[REGISTERS]={0};
    double error[REGISTERS]={0};
    int t0;
    
    //std::cout << "error ";
    //for(int j=0;j<INSTRUCTIONS;j++) 
   // {
   //     std::cout << x[j] << " ";
   // }
    //std::cout << std::endl;

    for(int j=0;j<INSTRUCTIONS;j++) 
    {
	t0 = (int)x[j];

	if(opcode[j]==LOG) {
        bits[dest[j]] = t0;
        log_rule(temp_lo[src0[j]], temp_hi[src0[j]], &temp_lo[dest[j]], &temp_hi[dest[j]]);
        log_errrule(temp_lo[src0[j]], temp_hi[src0[j]], error[src0[j]], t0, &error[dest[j]]);
    } else if(opcode[j]==EXP) {
            bits[dest[j]] = t0;
            //printf("EXP bits (error) : %d \n", bits[src0[j]]);
            exp_rule(temp_lo[src0[j]], temp_hi[src0[j]], &temp_lo[dest[j]], &temp_hi[dest[j]]);
            exp_errrule(temp_lo[src0[j]], temp_hi[src0[j]], error[src0[j]], t0, &error[dest[j]]);
    } else if(opcode[j]==DIV) {
        bits[dest[j]] = t0;
        div_rule(temp_lo[src0[j]], temp_hi[src0[j]],  temp_lo[src1[j]], temp_hi[src1[j]], &temp_lo[dest[j]], &temp_hi[dest[j]]);
        div_errrule(temp_lo[src0[j]], temp_hi[src0[j]], error[src0[j]], temp_lo[src1[j]], temp_hi[src1[j]], error[src1[j]], t0, &error[dest[j]]);
	} else if(opcode[j]==MUL) {
            bits[dest[j]] = t0;
            //printf("MUL1 bits (error): %d \n", bits[src0[j]]);
            //printf("MUL2 bits (error): %d \n", bits[src1[j]]);
            //printf("mult bitwidth is %d  ", t0);
            mult_rule(temp_lo[src0[j]], temp_hi[src0[j]], temp_lo[src1[j]], temp_hi[src1[j]], &temp_lo[dest[j]], &temp_hi[dest[j]]);
            //printf("temp_lo_0: %f, temp_hi_0: %f \n", temp_lo[src0[j]], temp_hi[src0[j]]);
            //printf("temp_lo_1: %f, temp_hi_1: %f \n", temp_lo[src1[j]], temp_hi[src1[j]]);
            mult_errrule(temp_lo[src0[j]], temp_hi[src0[j]], error[src0[j]], temp_lo[src1[j]], temp_hi[src1[j]], error[src1[j]], t0, &error[dest[j]]);
            //printf("mult error is %f  ", error[dest[j]]);
    } else if(opcode[j]==ADD) {
        bits[dest[j]] = t0;
        add_rule(temp_lo[src0[j]], temp_hi[src0[j]], temp_lo[src1[j]], temp_hi[src1[j]], &temp_lo[dest[j]], &temp_hi[dest[j]]);
        add_errrule(temp_lo[src0[j]], temp_hi[src0[j]], error[src0[j]], temp_lo[src1[j]], temp_hi[src1[j]], error[src1[j]], t0, &error[dest[j]]);
            //printf("add error is %f  ", error[dest[j]]);
	} else if(opcode[j]==SUB) {
            bits[dest[j]] = t0;
            sub_rule(temp_lo[src0[j]], temp_hi[src0[j]], temp_lo[src1[j]], temp_hi[src1[j]], &temp_lo[dest[j]], &temp_hi[dest[j]]);
            sub_errrule(temp_lo[src0[j]], temp_hi[src0[j]], error[src0[j]], temp_lo[src1[j]], temp_hi[src1[j]], error[src1[j]], t0, &error[dest[j]]);
            //printf("sub error is %f  ", error[dest[j]]);
	} else if(opcode[j]==LD) {
        //printf("LD bits (error): %d \n", bits[dest[j]]);
	    double in_hi_temp=temp_hi[dest[j]];
	    double in_lo_temp=temp_lo[dest[j]];
	    if( fabs(in_hi_temp-in_lo_temp) < 1e-30 ) 
        {
            double shift = in_lo_temp*pow(2,t0);
            double intpart;
            double fractpart = modf(shift, &intpart);
            if(fractpart > 1e-30)
                // unify error modeling for frac numbers
                error[dest[j]] = pow(2,-(t0+1));
                //error[dest[j]]=fractpart * pow(2,-(t0+1));
            else 
                error[dest[j]] = 0;
        } 
        else 
        {
            error[dest[j]]=pow(2,-(t0+1));
		}
        bits[dest[j]] = t0;
	} else if(opcode[j]==ST) {
	    out_err.push_back( error[src0[j]] );
        double ERROR_THRESH = pow(2, -9);
        bool valid = (out_err[j] <= ERROR_THRESH);
        //printf("final error is %e, %e, %d\n", out_err, ERROR_THRESH, valid);
	}
    }
    return out_err;
} 

#endif
