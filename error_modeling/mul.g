# tdfc-gappa backend autocompiled body file
# tdfc version 1.160
# Wed Aug 27 20:01:06 2014

@fx1 = fixed<-1,ne>;
@fx2 = fixed<-1,ne>;
@fx3 = fixed<-0,ne>;
@fx4 = fixed<-1,ne>;

a_m = (a);
b_m = (b);
plus_5_m  = (a_m * b_m);
c_m  =  (  ( plus_5_m  ) );

a_fx = fx1(a);
b_fx = fx2(b);
plus_5_fx  = fx3 (a_fx * b_fx);
c_fx = (  ( plus_5_fx  ) );


{

		a in [0,1] /\ 
		b in [0,1]  
		->
		(c_m-c_fx) in ? 
}

#add
