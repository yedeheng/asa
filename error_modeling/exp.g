# tdfc-gappa backend autocompiled body file
# tdfc version 1.160
# Wed Aug 27 20:01:06 2014

@fx1 = fixed<-1,ne>;
@fx2 = fixed<-2,ne>;
@fx3 = fixed<-3,ne>;
@fx4 = fixed<-2,ne>;

a_m = (a);
exp_5_m  = exp(a_m) ;
c_m  =  (  ( exp_5_m  ) );

a_fx = fx1(a);
exp_5_fx  = fx3 (exp(a_fx));
c_fx = fx4 (  ( exp_5_fx  ) );


{

		a in [3,4]  
		->
        a_fx - a_m in ?/\
        exp_5_fx - exp_5_m in ?/\
		(c_fx-c_m) in ? 
}

#add
