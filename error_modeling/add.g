# tdfc-gappa backend autocompiled body file
# tdfc version 1.160
# Wed Aug 27 20:01:06 2014

@fx1 = fixed<-9,ne>;
@fx2 = fixed<-0,ne>;
@fx3 = fixed<-64,ne>;
@fx4 = fixed<-64,ne>;

a_m = (a);
b_m = (b);
plus_5_m  = (a_m + b_m);
c_m  =  (  ( plus_5_m  ) );

a_dbl = float<ieee_64,ne>(a);
b_dbl = float<ieee_64,ne>(b);
plus_5_dbl float<ieee_64,ne> = (a_dbl + b_dbl);
c_dbl float<ieee_64,ne> =  (  ( plus_5_dbl  ) );

a_fx = fx1(a);
b_fx = fx2(b);
plus_5_fx  = fx3 (a_fx + b_fx);
c_fx = fx4 (  ( plus_5_fx  ) );


{

		a in [3,4] /\ 
		b in [1,2] /\ 
		
		( |c_m | >= 0x1p-53 )

		->
		(c_dbl-c_m)/c_m in ? /\ 
		(c_fx-c_m)/c_m in ? /\ 
		(c_m-c_fx) in ? /\ 
		(c_m-c_dbl) in ?
}

#add
