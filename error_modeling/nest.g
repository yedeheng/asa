# tdfc-gappa backend autocompiled body file
# tdfc version 1.160
# Wed Aug 27 20:01:06 2014

@fx1 = fixed<-1,ne>;
@fx2 = fixed<-0,ne>;

a_m = (a);

a_1 = fx1(a);
a_2 = fx2(a_1);

{

		a in [3,4]  
		->
        (a_1 - a) in ?/\
        (a_2 - a) in ?
}

#add
