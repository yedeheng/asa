# tdfc-gappa backend autocompiled body file
# tdfc version 1.160
# Tue Jul 22 16:23:40 2014

@fx1 = fixed<-64,ne>;
@fx2 = fixed<-64,ne>;
@fx3 = fixed<-64,ne>;
@fx4 = fixed<-64,ne>;
@fx5 = fixed<-64,ne>;
@fx6 = fixed<-64,ne>;
@fx7 = fixed<-64,ne>;
@fx8 = fixed<-64,ne>;
@fx9 = fixed<-64,ne>;
@fx10 = fixed<-64,ne>;

x_m = float<ieee_64,ne>(x);
divide_9_m  = (x_m / 4);
divide_10_m  = (1 / 3);
plus_12_m  = (divide_9_m + divide_10_m);
multiply_13_m  = (x_m * plus_12_m);
minus_14_m  = (0.5 - multiply_13_m);
multiply_15_m  = (x_m * minus_14_m);
minus_16_m  = (1 - multiply_15_m);
multiply_17_m  = (x_m * minus_16_m);
y_m  =  (  ( multiply_17_m  ) );

x_dbl = float<ieee_64,ne>(x);
divide_9_dbl float<ieee_64,ne> = (x_dbl / 4);
divide_10_dbl float<ieee_64,ne> = (1 / 3);
plus_12_dbl float<ieee_64,ne> = (divide_9_dbl + divide_10_dbl);
multiply_13_dbl float<ieee_64,ne> = (x_dbl * plus_12_dbl);
minus_14_dbl float<ieee_64,ne> = (0.5 - multiply_13_dbl);
multiply_15_dbl float<ieee_64,ne> = (x_dbl * minus_14_dbl);
minus_16_dbl float<ieee_64,ne> = (1 - multiply_15_dbl);
multiply_17_dbl float<ieee_64,ne> = (x_dbl * minus_16_dbl);
y_dbl float<ieee_64,ne> =  (  ( multiply_17_dbl  ) );

x_fx = fx1(x);
divide_9_fx  = fx2 (x_fx / 4);
divide_10_fx  = fx3 (1 / 3);
plus_12_fx  = fx4 (divide_9_fx + divide_10_fx);
multiply_13_fx  = fx5 (x_fx * plus_12_fx);
minus_14_fx  = fx6 (0.5 - multiply_13_fx);
multiply_15_fx  = fx7 (x_fx * minus_14_fx);
minus_16_fx  = fx8 (1 - multiply_15_fx);
multiply_17_fx  = fx9 (x_fx * minus_16_fx);
y_fx = fx10 (  ( multiply_17_fx  ) );


{

		x in [0,1] /\ 
		
		( |y_m | >= 0x1p-53 )

		->
		(y_dbl-y_m)/y_m in ? /\ 
		(y_fx-y_m)/y_m in ? /\ 
		(y_m-y_fx) in ? /\ 
		(y_m-y_dbl) in ?
}

#poly
