// ccautoesl autocompiled header file
// tdfc version 1.160
// Thu Jul 24 20:35:49 2014


#ifndef poly_H_
#define poly_H_
//#define SC_INCLUDE_FX
#include "ap_fixed.h"
//#include "systemc.h"

typedef ap_fixed<64,8> data_t_const1;
typedef ap_fixed<64,8> data_t_const2;
typedef ap_fixed<64,8> data_t_const3;
typedef ap_fixed<64,8> data_t_const4;
typedef ap_fixed<64,8> data_t_io1;
typedef ap_fixed<64,8> data_t1;
typedef ap_fixed<64,8> data_t2;
typedef ap_fixed<64,8> data_t3;
typedef ap_fixed<64,8> data_t4;
typedef ap_fixed<64,8> data_t5;
typedef ap_fixed<64,8> data_t6;
typedef ap_fixed<64,8> data_t7;
typedef ap_fixed<64,8> data_t_io2;
#endif /*poly_H_*/

