// ccautoesl autocompiled header file
// tdfc version 1.160
// Fri Jul 25 17:12:46 2014

#ifndef poly_H_
#define poly_H_
#include "ap_fixed.h"
#include "asa_generated_header.h"

typedef ap_fixed<VAR1_BITS+8,8> data_t_const1;
typedef ap_fixed<VAR2_BITS+8,8> data_t_const2;
typedef ap_fixed<VAR3_BITS+8,8> data_t_const3;
typedef ap_fixed<VAR4_BITS+8,8> data_t_const4;
typedef ap_fixed<VAR5_BITS+8,8> data_t_io1;
typedef ap_fixed<VAR6_BITS+8,8> data_t1;
typedef ap_fixed<VAR7_BITS+8,8> data_t2;
typedef ap_fixed<VAR8_BITS+8,8> data_t3;
typedef ap_fixed<VAR9_BITS+8,8> data_t4;
typedef ap_fixed<VAR10_BITS+8,8> data_t5;
typedef ap_fixed<VAR11_BITS+8,8> data_t6;
typedef ap_fixed<VAR12_BITS+8,8> data_t7;
typedef ap_fixed<VAR13_BITS+8,8> data_t_io2;
#endif /*poly_H_*/

