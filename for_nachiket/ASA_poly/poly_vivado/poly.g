# tdfc-gappa backend autocompiled body file
# tdfc version 1.160
# Fri Jul 25 17:12:46 2014

@fx1 = fixed<-55,ne>;
@fx2 = fixed<-55,ne>;
@fx3 = fixed<-55,ne>;
@fx4 = fixed<-55,ne>;
@fx5 = fixed<-55,ne>;
@fx6 = fixed<-55,ne>;
@fx7 = fixed<-55,ne>;
@fx8 = fixed<-55,ne>;
@fx9 = fixed<-55,ne>;
@fx10 = fixed<-55,ne>;
@fx11 = fixed<-55,ne>;
@fx12 = fixed<-55,ne>;
@fx13 = fixed<-55,ne>;

c1_m = float<ieee_64,ne>(0.100000);
c2_m = float<ieee_64,ne>(0.200000);
c3_m = float<ieee_64,ne>(0.300000);
c4_m = float<ieee_64,ne>(0.400000);
x_m = float<ieee_64,ne>(x);
multiply_8_m  = (x_m * c3_m);
plus_10_m  = (multiply_8_m + c4_m);
multiply_11_m  = (x_m * plus_10_m);
minus_12_m  = (c2_m - multiply_11_m);
multiply_13_m  = (x_m * minus_12_m);
minus_14_m  = (c1_m - multiply_13_m);
multiply_15_m  = (x_m * minus_14_m);
y_m  =  (  ( multiply_15_m  ) );

c1_dbl = float<ieee_64,ne>(0.100000);
c2_dbl = float<ieee_64,ne>(0.200000);
c3_dbl = float<ieee_64,ne>(0.300000);
c4_dbl = float<ieee_64,ne>(0.400000);
x_dbl = float<ieee_64,ne>(x);
multiply_8_dbl float<ieee_64,ne> = (x_dbl * c3_dbl);
plus_10_dbl float<ieee_64,ne> = (multiply_8_dbl + c4_dbl);
multiply_11_dbl float<ieee_64,ne> = (x_dbl * plus_10_dbl);
minus_12_dbl float<ieee_64,ne> = (c2_dbl - multiply_11_dbl);
multiply_13_dbl float<ieee_64,ne> = (x_dbl * minus_12_dbl);
minus_14_dbl float<ieee_64,ne> = (c1_dbl - multiply_13_dbl);
multiply_15_dbl float<ieee_64,ne> = (x_dbl * minus_14_dbl);
y_dbl float<ieee_64,ne> =  (  ( multiply_15_dbl  ) );

c1_fx = fx1(0.100000);
c2_fx = fx2(0.200000);
c3_fx = fx3(0.300000);
c4_fx = fx4(0.400000);
x_fx = fx5(x);
multiply_8_fx  = fx6 (x_fx * c3_fx);
plus_10_fx  = fx7 (multiply_8_fx + c4_fx);
multiply_11_fx  = fx8 (x_fx * plus_10_fx);
minus_12_fx  = fx9 (c2_fx - multiply_11_fx);
multiply_13_fx  = fx10 (x_fx * minus_12_fx);
minus_14_fx  = fx11 (c1_fx - multiply_13_fx);
multiply_15_fx  = fx12 (x_fx * minus_14_fx);
y_fx = fx13 (  ( multiply_15_fx  ) );


{

		x in [0,1] /\ 
		
		( |y_m | >= 0x1p-53 )

		->
(y_m-y_fx) in ? /\ 
		(y_m-y_dbl) in ?
}

#poly
