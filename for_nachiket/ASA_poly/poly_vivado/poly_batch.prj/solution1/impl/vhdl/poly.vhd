-- ==============================================================
-- RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC
-- Version: 2013.2
-- Copyright (C) 2013 Xilinx Inc. All rights reserved.
-- 
-- ===========================================================

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity poly is
port (
    ap_clk : IN STD_LOGIC;
    ap_rst : IN STD_LOGIC;
    ap_start : IN STD_LOGIC;
    ap_done : OUT STD_LOGIC;
    ap_idle : OUT STD_LOGIC;
    ap_ready : OUT STD_LOGIC;
    cc_x_fx_V : IN STD_LOGIC_VECTOR (71 downto 0);
    cc_y_fx_V : OUT STD_LOGIC_VECTOR (71 downto 0);
    cc_y_fx_V_ap_vld : OUT STD_LOGIC );
end;


architecture behav of poly is 
    attribute CORE_GENERATION_INFO : STRING;
    attribute CORE_GENERATION_INFO of behav : architecture is
    "poly,hls_ip_2013_2,{HLS_INPUT_TYPE=cxx,HLS_INPUT_FLOAT=0,HLS_INPUT_FIXED=1,HLS_INPUT_PART=xc7k160tfbg484-2,HLS_INPUT_CLOCK=3.000000,HLS_INPUT_ARCH=others,HLS_SYN_CLOCK=2.200000,HLS_SYN_LAT=8,HLS_SYN_TPT=none,HLS_SYN_MEM=0,HLS_SYN_DSP=51,HLS_SYN_FF=499,HLS_SYN_LUT=215}";
    constant ap_const_logic_1 : STD_LOGIC := '1';
    constant ap_const_logic_0 : STD_LOGIC := '0';
    constant ap_ST_st1_fsm_0 : STD_LOGIC_VECTOR (3 downto 0) := "0000";
    constant ap_ST_st2_fsm_1 : STD_LOGIC_VECTOR (3 downto 0) := "0001";
    constant ap_ST_st3_fsm_2 : STD_LOGIC_VECTOR (3 downto 0) := "0010";
    constant ap_ST_st4_fsm_3 : STD_LOGIC_VECTOR (3 downto 0) := "0011";
    constant ap_ST_st5_fsm_4 : STD_LOGIC_VECTOR (3 downto 0) := "0100";
    constant ap_ST_st6_fsm_5 : STD_LOGIC_VECTOR (3 downto 0) := "0101";
    constant ap_ST_st7_fsm_6 : STD_LOGIC_VECTOR (3 downto 0) := "0110";
    constant ap_ST_st8_fsm_7 : STD_LOGIC_VECTOR (3 downto 0) := "0111";
    constant ap_ST_st9_fsm_8 : STD_LOGIC_VECTOR (3 downto 0) := "1000";
    constant ap_const_lv32_2 : STD_LOGIC_VECTOR (31 downto 0) := "00000000000000000000000000000010";
    constant ap_const_lv32_47 : STD_LOGIC_VECTOR (31 downto 0) := "00000000000000000000000001000111";
    constant ap_const_lv71_4CCCCCCCCCCCCC00 : STD_LOGIC_VECTOR (70 downto 0) := "00000000100110011001100110011001100110011001100110011001100110000000000";
    constant ap_const_lv32_40 : STD_LOGIC_VECTOR (31 downto 0) := "00000000000000000000000001000000";
    constant ap_const_lv32_87 : STD_LOGIC_VECTOR (31 downto 0) := "00000000000000000000000010000111";
    constant ap_const_lv72_8000000000000000 : STD_LOGIC_VECTOR (71 downto 0) := "000000001000000000000000000000000000000000000000000000000000000000000000";
    constant ap_const_lv72_10000000000000000 : STD_LOGIC_VECTOR (71 downto 0) := "000000010000000000000000000000000000000000000000000000000000000000000000";

    signal ap_CS_fsm : STD_LOGIC_VECTOR (3 downto 0) := "0000";
    signal plus_10_fx_V_fu_54_p2 : STD_LOGIC_VECTOR (70 downto 0);
    signal plus_10_fx_V_reg_134 : STD_LOGIC_VECTOR (70 downto 0);
    signal OP1_V_cast_fu_60_p1 : STD_LOGIC_VECTOR (135 downto 0);
    signal cc_x_fx_V_temp: signed (72-1 downto 0);
    signal OP1_V_cast_reg_139 : STD_LOGIC_VECTOR (135 downto 0);
    signal multiply_11_fx_V_reg_151 : STD_LOGIC_VECTOR (71 downto 0);
    signal minus_12_fx_V_fu_82_p2 : STD_LOGIC_VECTOR (71 downto 0);
    signal minus_12_fx_V_reg_156 : STD_LOGIC_VECTOR (71 downto 0);
    signal multiply_13_fx_V_reg_166 : STD_LOGIC_VECTOR (71 downto 0);
    signal minus_14_fx_V_fu_105_p2 : STD_LOGIC_VECTOR (71 downto 0);
    signal minus_14_fx_V_reg_171 : STD_LOGIC_VECTOR (71 downto 0);
    signal tmp_2_fu_40_p4 : STD_LOGIC_VECTOR (69 downto 0);
    signal plus_10_fx_V_fu_54_p0 : STD_LOGIC_VECTOR (70 downto 0);
    signal tmp_2_fu_40_p4_temp: signed (70-1 downto 0);
    signal grp_fu_66_p0 : STD_LOGIC_VECTOR (70 downto 0);
    signal grp_fu_66_p1 : STD_LOGIC_VECTOR (71 downto 0);
    signal grp_fu_66_p2 : STD_LOGIC_VECTOR (135 downto 0);
    signal grp_fu_90_p0 : STD_LOGIC_VECTOR (71 downto 0);
    signal grp_fu_90_p1 : STD_LOGIC_VECTOR (71 downto 0);
    signal grp_fu_90_p2 : STD_LOGIC_VECTOR (135 downto 0);
    signal grp_fu_113_p0 : STD_LOGIC_VECTOR (71 downto 0);
    signal grp_fu_113_p1 : STD_LOGIC_VECTOR (71 downto 0);
    signal grp_fu_113_p2 : STD_LOGIC_VECTOR (135 downto 0);
    signal grp_fu_66_ce : STD_LOGIC;
    signal grp_fu_90_ce : STD_LOGIC;
    signal grp_fu_113_ce : STD_LOGIC;
    signal ap_NS_fsm : STD_LOGIC_VECTOR (3 downto 0);

    component poly_mul_71s_72s_136_2 IS
    generic (
        ID : INTEGER;
        NUM_STAGE : INTEGER;
        din0_WIDTH : INTEGER;
        din1_WIDTH : INTEGER;
        dout_WIDTH : INTEGER );
    port (
        clk : IN STD_LOGIC;
        reset : IN STD_LOGIC;
        din0 : IN STD_LOGIC_VECTOR (70 downto 0);
        din1 : IN STD_LOGIC_VECTOR (71 downto 0);
        ce : IN STD_LOGIC;
        dout : OUT STD_LOGIC_VECTOR (135 downto 0) );
    end component;


    component poly_mul_72s_72s_136_2 IS
    generic (
        ID : INTEGER;
        NUM_STAGE : INTEGER;
        din0_WIDTH : INTEGER;
        din1_WIDTH : INTEGER;
        dout_WIDTH : INTEGER );
    port (
        clk : IN STD_LOGIC;
        reset : IN STD_LOGIC;
        din0 : IN STD_LOGIC_VECTOR (71 downto 0);
        din1 : IN STD_LOGIC_VECTOR (71 downto 0);
        ce : IN STD_LOGIC;
        dout : OUT STD_LOGIC_VECTOR (135 downto 0) );
    end component;



begin
    poly_mul_71s_72s_136_2_U1 : component poly_mul_71s_72s_136_2
    generic map (
        ID => 1,
        NUM_STAGE => 2,
        din0_WIDTH => 71,
        din1_WIDTH => 72,
        dout_WIDTH => 136)
    port map (
        clk => ap_clk,
        reset => ap_rst,
        din0 => grp_fu_66_p0,
        din1 => grp_fu_66_p1,
        ce => grp_fu_66_ce,
        dout => grp_fu_66_p2);

    poly_mul_72s_72s_136_2_U2 : component poly_mul_72s_72s_136_2
    generic map (
        ID => 2,
        NUM_STAGE => 2,
        din0_WIDTH => 72,
        din1_WIDTH => 72,
        dout_WIDTH => 136)
    port map (
        clk => ap_clk,
        reset => ap_rst,
        din0 => grp_fu_90_p0,
        din1 => grp_fu_90_p1,
        ce => grp_fu_90_ce,
        dout => grp_fu_90_p2);

    poly_mul_72s_72s_136_2_U3 : component poly_mul_72s_72s_136_2
    generic map (
        ID => 3,
        NUM_STAGE => 2,
        din0_WIDTH => 72,
        din1_WIDTH => 72,
        dout_WIDTH => 136)
    port map (
        clk => ap_clk,
        reset => ap_rst,
        din0 => grp_fu_113_p0,
        din1 => grp_fu_113_p1,
        ce => grp_fu_113_ce,
        dout => grp_fu_113_p2);




    -- the current state (ap_CS_fsm) of the state machine. --
    ap_CS_fsm_assign_proc : process(ap_clk)
    begin
        if (ap_clk'event and ap_clk =  '1') then
            if (ap_rst = '1') then
                ap_CS_fsm <= ap_ST_st1_fsm_0;
            else
                ap_CS_fsm <= ap_NS_fsm;
            end if;
        end if;
    end process;


    -- assign process. --
    process (ap_clk)
    begin
        if (ap_clk'event and ap_clk = '1') then
            if ((ap_ST_st2_fsm_1 = ap_CS_fsm)) then
                OP1_V_cast_reg_139 <= OP1_V_cast_fu_60_p1;
            end if;
        end if;
    end process;

    -- assign process. --
    process (ap_clk)
    begin
        if (ap_clk'event and ap_clk = '1') then
            if ((ap_ST_st4_fsm_3 = ap_CS_fsm)) then
                minus_12_fx_V_reg_156 <= minus_12_fx_V_fu_82_p2;
            end if;
        end if;
    end process;

    -- assign process. --
    process (ap_clk)
    begin
        if (ap_clk'event and ap_clk = '1') then
            if ((ap_ST_st7_fsm_6 = ap_CS_fsm)) then
                minus_14_fx_V_reg_171 <= minus_14_fx_V_fu_105_p2;
            end if;
        end if;
    end process;

    -- assign process. --
    process (ap_clk)
    begin
        if (ap_clk'event and ap_clk = '1') then
            if ((ap_ST_st3_fsm_2 = ap_CS_fsm)) then
                multiply_11_fx_V_reg_151 <= grp_fu_66_p2(135 downto 64);
            end if;
        end if;
    end process;

    -- assign process. --
    process (ap_clk)
    begin
        if (ap_clk'event and ap_clk = '1') then
            if ((ap_ST_st6_fsm_5 = ap_CS_fsm)) then
                multiply_13_fx_V_reg_166 <= grp_fu_90_p2(135 downto 64);
            end if;
        end if;
    end process;

    -- assign process. --
    process (ap_clk)
    begin
        if (ap_clk'event and ap_clk = '1') then
            if (((ap_ST_st1_fsm_0 = ap_CS_fsm) and not((ap_start = ap_const_logic_0)))) then
                plus_10_fx_V_reg_134 <= plus_10_fx_V_fu_54_p2;
            end if;
        end if;
    end process;

    -- the next state (ap_NS_fsm) of the state machine. --
    ap_NS_fsm_assign_proc : process (ap_start , ap_CS_fsm)
    begin
        case ap_CS_fsm is
            when ap_ST_st1_fsm_0 => 
                if (not((ap_start = ap_const_logic_0))) then
                    ap_NS_fsm <= ap_ST_st2_fsm_1;
                else
                    ap_NS_fsm <= ap_ST_st1_fsm_0;
                end if;
            when ap_ST_st2_fsm_1 => 
                ap_NS_fsm <= ap_ST_st3_fsm_2;
            when ap_ST_st3_fsm_2 => 
                ap_NS_fsm <= ap_ST_st4_fsm_3;
            when ap_ST_st4_fsm_3 => 
                ap_NS_fsm <= ap_ST_st5_fsm_4;
            when ap_ST_st5_fsm_4 => 
                ap_NS_fsm <= ap_ST_st6_fsm_5;
            when ap_ST_st6_fsm_5 => 
                ap_NS_fsm <= ap_ST_st7_fsm_6;
            when ap_ST_st7_fsm_6 => 
                ap_NS_fsm <= ap_ST_st8_fsm_7;
            when ap_ST_st8_fsm_7 => 
                ap_NS_fsm <= ap_ST_st9_fsm_8;
            when ap_ST_st9_fsm_8 => 
                ap_NS_fsm <= ap_ST_st1_fsm_0;
            when others =>  
                ap_NS_fsm <= "XXXX";
        end case;
    end process;
    
    cc_x_fx_V_temp <= signed(cc_x_fx_V);
    OP1_V_cast_fu_60_p1 <= std_logic_vector(resize(cc_x_fx_V_temp,136));


    -- ap_done assign process. --
    ap_done_assign_proc : process(ap_CS_fsm)
    begin
        if ((ap_ST_st9_fsm_8 = ap_CS_fsm)) then 
            ap_done <= ap_const_logic_1;
        else 
            ap_done <= ap_const_logic_0;
        end if; 
    end process;


    -- ap_idle assign process. --
    ap_idle_assign_proc : process(ap_start, ap_CS_fsm)
    begin
        if ((not((ap_const_logic_1 = ap_start)) and (ap_ST_st1_fsm_0 = ap_CS_fsm))) then 
            ap_idle <= ap_const_logic_1;
        else 
            ap_idle <= ap_const_logic_0;
        end if; 
    end process;


    -- ap_ready assign process. --
    ap_ready_assign_proc : process(ap_CS_fsm)
    begin
        if ((ap_ST_st9_fsm_8 = ap_CS_fsm)) then 
            ap_ready <= ap_const_logic_1;
        else 
            ap_ready <= ap_const_logic_0;
        end if; 
    end process;

    cc_y_fx_V <= grp_fu_113_p2(135 downto 64);

    -- cc_y_fx_V_ap_vld assign process. --
    cc_y_fx_V_ap_vld_assign_proc : process(ap_CS_fsm)
    begin
        if ((ap_ST_st9_fsm_8 = ap_CS_fsm)) then 
            cc_y_fx_V_ap_vld <= ap_const_logic_1;
        else 
            cc_y_fx_V_ap_vld <= ap_const_logic_0;
        end if; 
    end process;

    grp_fu_113_ce <= ap_const_logic_1;
    grp_fu_113_p0 <= minus_14_fx_V_reg_171;
    grp_fu_113_p1 <= OP1_V_cast_reg_139(72 - 1 downto 0);
    grp_fu_66_ce <= ap_const_logic_1;
    grp_fu_66_p0 <= plus_10_fx_V_reg_134;
    grp_fu_66_p1 <= cc_x_fx_V;
    grp_fu_90_ce <= ap_const_logic_1;
    grp_fu_90_p0 <= minus_12_fx_V_reg_156;
    grp_fu_90_p1 <= OP1_V_cast_reg_139(72 - 1 downto 0);
    minus_12_fx_V_fu_82_p2 <= std_logic_vector(unsigned(ap_const_lv72_8000000000000000) - unsigned(multiply_11_fx_V_reg_151));
    minus_14_fx_V_fu_105_p2 <= std_logic_vector(unsigned(ap_const_lv72_10000000000000000) - unsigned(multiply_13_fx_V_reg_166));
    
    tmp_2_fu_40_p4_temp <= signed(tmp_2_fu_40_p4);
    plus_10_fx_V_fu_54_p0 <= std_logic_vector(resize(tmp_2_fu_40_p4_temp,71));

    plus_10_fx_V_fu_54_p2 <= std_logic_vector(unsigned(plus_10_fx_V_fu_54_p0) + unsigned(ap_const_lv71_4CCCCCCCCCCCCC00));
    tmp_2_fu_40_p4 <= cc_x_fx_V(71 downto 2);
end behav;
