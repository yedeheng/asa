set moduleName poly
set isCombinational 0
set isDatapathOnly 0
set isPipelined 0
set pipeline_type none
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set C_modelName poly
set C_modelType { void 0 }
set C_modelArgList { 
	{ cc_x_fx_V int 72 regular  }
	{ cc_y_fx_V int 72 regular {pointer 1}  }
}

# RTL Port declarations: 
set portNum 9
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ cc_x_fx_V sc_in sc_lv 72 signal 0 } 
	{ cc_y_fx_V sc_out sc_lv 72 signal 1 } 
	{ cc_y_fx_V_ap_vld sc_out sc_logic 1 outvld 1 } 
}

set Spec2ImplPortList { 
	cc_x_fx_V { ap_none {  { cc_x_fx_V in_data 0 72 } } }
	cc_y_fx_V { ap_vld {  { cc_y_fx_V out_data 1 72 }  { cc_y_fx_V_ap_vld out_vld 1 1 } } }
}

# RTL port scheduling information:
set fifoSchedulingInfoList { 
}

# RTL bus port read request latency information:
set busReadReqLatencyList { 
}
