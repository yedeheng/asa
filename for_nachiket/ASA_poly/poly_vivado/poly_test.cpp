// tdfc-autoesl autocompiled wrapper file
// tdfc version 1.160
// Fri Jul 25 17:12:46 2014

#include <stdio.h>
#include <math.h>
#include "poly.h"

int main()
{
  int N=1024;
  const double cc_c1 = (double)1;
  const double cc_c2 = (double)1;
  const double cc_c3 = (double)1;
  const double cc_c4 = (double)1;
  double *cc_x;
  double *cc_y;

  // cc_x
  size_t cc_x_size = N * sizeof(double);
  cc_x = (double *)malloc(cc_x_size);

  // cc_y
  size_t cc_y_size = N * sizeof(double);
  cc_y = (double *)malloc(cc_y_size);

  // Initialize input contents and copy to Device memory
  for(int i=0; i<N; i++) {
    cc_x[i] = (double)i;
  }

  // Loop
  for(int i=0; i<N; i++) {
    poly (
      cc_c1,
      cc_c2,
      cc_c3,
      cc_c4,
      cc_x[i],
      &cc_y[i]
    );
  }

  // Print results (typecasted to prevent printf errors)
  printf("poly:\n");
  for (int i=0; i<N; i++) printf("cc_y[%d] = %g\n", i, cc_y[i]);

  // Cleanup
  free(cc_x); 
  free(cc_y); 

} // main
