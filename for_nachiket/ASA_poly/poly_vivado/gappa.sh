#!/bin/zsh
bmk=poly

cd ~/workspace/ASA_${bmk}/${bmk}_vivado

# cat asa_generated_file.g > new.g
# cat ${bmk}.g >> new.g

gappa ${bmk}.g &> ${bmk}_gappa.log
cat  ${bmk}_gappa.log | grep -a "_m.*\[" | grep -a -m 1 _fx | sed "s/.* in \[\(.*\)\, \(.*\)\]/\1/"
cat  ${bmk}_gappa.log | grep -a "_m.*\[" | grep -a -m 1 _fx | sed "s/.* in \[\(.*\)\, \(.*\)\]/\2/"
cat  ${bmk}_gappa.log | grep -a "_m.*\[" | grep -a -m 1 _dbl | sed "s/.* in \[\(.*\)\, \(.*\)\]/\1/"
cat  ${bmk}_gappa.log | grep -a "_m.*\[" | grep -a -m 1 _dbl | sed "s/.* in \[\(.*\)\, \(.*\)\]/\2/"
#echo "$low" > low_constraint.dat
#echo "$high" > high_constraint.dat
