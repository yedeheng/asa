#!/bin/zsh
bmk=poly
dir=~/workspace/ASA_${bmk}/${bmk}_vivado

cd  $dir
rm -rf ${bmk}_batch.prj

# HLS: C -> RTL
#vivado_hls -f $bmk.tcl &> /dev/null
make -f Makefile.$bmk autoesl &> /dev/null

# capture area(LUT) data from HLS report
cat  ${bmk}_batch.prj/solution1/syn/report/${bmk}_csynth.rpt | grep "Utilization Estimates" -A 14 | tail -n 1 | cut -d"|" -f 6 | sed "s/\s//g"

# Stop at logic synthesis
#sed -i -e "/timing_synth.rpt/a exit" ${bmk}_batch.prj/solution1/impl/vhdl/run_vivado.tcl

# hardware generation
#make -f Makefile.$bmk hardware &> /dev/null

# capture area(LUT) data from logic syntheis report
#cat ${bmk}_batch.prj/solution1/impl/vhdl/report/${bmk}_utilization_synth.rpt | grep "Slice LUTs" | cut -d "|" -f 3 | sed "s/\s//g"

cd ..
