// tdfc-autoesl backend autocompiled body file
// tdfc version 1.160
// Fri Jul 25 17:12:46 2014

#include "math.h"
#include "poly.h"

void poly(
	data_t_io1 cc_x_fx,
	data_t_io2 *cc_y_fx
)
{
    data_t_const1 cc_c1_fx = (0.100000);
    data_t_const2 cc_c2_fx = (0.200000);
    data_t_const3 cc_c3_fx = (0.300000);
    data_t_const4 cc_c4_fx = (0.400000);
{ 
data_t1 multiply_8_fx  = (cc_x_fx * cc_c3_fx);
data_t2 plus_10_fx  = (multiply_8_fx + cc_c4_fx);
data_t3 multiply_11_fx  = (cc_x_fx * plus_10_fx);
data_t4 minus_12_fx  = (cc_c2_fx - multiply_11_fx);
data_t5 multiply_13_fx  = (cc_x_fx * minus_12_fx);
data_t6 minus_14_fx  = (cc_c1_fx - multiply_13_fx);
data_t7 multiply_15_fx  = (cc_x_fx * minus_14_fx);
*cc_y_fx =  (  ( multiply_15_fx  ) );
	 // *cc_y = ((cc_x*(cc_c1-(cc_x*(cc_c2-(cc_x*((cc_x*cc_c3)+cc_c4)))))));
}
}
