#!/bin/zsh -i

file=`basename $1 .cpp`
cp $file.cpp $file.asm

dfg=`cat $file.cpp | grep "data_t\|=" | grep -v "data_t_io.*\*" | grep -v "\/\/"`
echo "$dfg" > $file.asm

sed -i "s/data_t_io[0-9]*\(.*\),/LD,\1,-1,-1/" $file.asm
sed -i "s/data_t_const[0-9]*\(.*\)=\(.*\);/LD,\1,\2,\2/" $file.asm
sed -i "s/data_t[0-9]*\(.*\)=\(.*\) + \(.*\);/ADD,\1,\2,\3/" $file.asm
sed -i "s/data_t[0-9]*\(.*\)=\(.*\) \* \(.*\);/MUL,\1,\2,\3/" $file.asm
sed -i "s/data_t[0-9]*\(.*\)=\(.*\) - \(.*\);/SUB,\1,\2,\3/" $file.asm
sed -i "s/*\(.*\)=\(.*\);/ST,\2,\2,-1/" $file.asm

sed -i "s/(//g" $file.asm
sed -i "s/)//g" $file.asm
sed -i "s/\ //g" $file.asm
sed -i "s/\t//g" $file.asm
sed -i "s/_fx//g" $file.asm
sed -i "s/cc_//g" $file.asm

#minimum=`cat $file.asm | sed "s/[a-z]*_\([0-9]*\)/\1/g" `
#echo $minimum
loads=`cat $file.asm | grep "LD" | cut -d"," -f2`
loads_cnt=`cat $file.asm | grep "LD" | wc -l`
sed -i "s/\(.*,\)[a-z]*_\([0-9]*\)\(.*\)/printf '\1';echo '\2 + $loads_cnt' \| bc \| tr '\n' ' '; printf '\3'/e"  $file.asm
sed -i "s/\(.*,\)[a-z]*_\([0-9]*\)\(.*\)/printf '\1';echo '\2 + $loads_cnt' \| bc \| tr '\n' ' '; printf '\3'/e"  $file.asm

cntr=0;
for ld in `echo $loads`
do
    sed -i "s/\<$ld\>/$cntr/g" $file.asm
    cntr=`echo $cntr + 1 | bc`
done

sed -i "s/\ //g" $file.asm

while read line
do
    opcode=`echo $line | cut -d' ' -f2`
    intcode=`echo $line | cut -d' ' -f3`
    sed -i "s/$opcode/$intcode/g" $file.asm
done < ../opcode.h
