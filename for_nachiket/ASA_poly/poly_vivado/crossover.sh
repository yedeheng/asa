#!/bin/zsh
if (($+1))
then 
else
    echo "usage: ./crossover.sh <benchmark_name.g>"
    exit
fi

for bits in {30..90}
do
    sed -i -e "s/fixed<-.*,/fixed<-${bits},/g" $1
    gappa $1 &> $1.log
    low_bound=`cat ${1}.log | grep -a "_m.*\[" | grep -a -m 1 _dbl | sed "s/.* in \[\(.*\)\, \(.*\)\]/\1/"`
    #echo $low_bound
    high_bound=`cat ${1}.log | grep -a "_m.*\[" | grep -a -m 1 _dbl | sed "s/.* in \[\(.*\)\, \(.*\)\]/\2/"`
    fx_error=`cat ${1}.log | grep -a "_m.*\[" | grep -a -m 1 _fx | sed "s/.* in \[\(.*\)\, \(.*\)\]/\2/"`
    success=`echo "$fx_error $high_bound" | awk '{print ($1 < $2)}'`
    if [ $success -eq 1 ]
    then 
	echo $bits
	break
    fi
done
