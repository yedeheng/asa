#!/bin/zsh

DESIGN=poly
MODEL=0

cd ${DESIGN}_vivado
tdfc -eautoesl 64 8 0 ${DESIGN}.tdfc
tdfc -egappa 64 ${DESIGN}.tdfc

./vivado_to_asm.sh ${DESIGN}.cpp
cp ./${DESIGN}.asm ..

cd ..

make clean
make DESIGN=$DESIGN MODEL=$MODEL
cat ./asa_usr_out
