#ifndef AREA_H_
#define AREA_H

#define REGISTERS 64
#include<vector>
#include<string>
#include "opcode.h"

double exp_area(int x) {
    double area = 2718.857 + 18.66216*x + 0.05235931*x*x - 4.570707e-04*x*x*x;
    return area;
}

double log_area(int x) {
    double area = 2032.929 + 19.08337*x + 0.05482684*x*x - 5.176768e-04*x*x*x;
    return area;
}

double mult_area(int x1, int x2) {
    double area = x1*x2;
    return area;
}

double sub_area(int x1, int x2) {
    return (x1>x2)?x1:x2 + 1;
}

double add_area(int x1, int x2) {
    return (x1>x2)?x1:x2;
}

int peace_area(std::vector<int> opcode, std::vector<int> src0, std::vector<int> src1, std::vector<int> dest, int INSTRUCTIONS, double* x)
{
    double area[REGISTERS];
    double area_total = 0;
    int t0;
    for(int j=0;j<INSTRUCTIONS;j++) 
    {
        t0 = (int)x[j];
        if(opcode[j]==LOG) {
            area_total += log_area(area[src0[j]]);
            area[dest[j]] = t0;
        } else if(opcode[j]==EXP) {
            area_total += exp_area(area[src0[j]]);
            area[dest[j]] = t0;
        } else if(opcode[j]==MUL) {
            area_total += mult_area(area[src0[j]], area[src1[j]]);
            area[dest[j]] = t0;
        } else if(opcode[j]==ADD) {
            area_total += add_area(area[src0[j]], area[src1[j]]);
            area[dest[j]] = t0;
            //printf("area total is : %f", area_total);
        } else if(opcode[j]==SUB) {
            area_total += sub_area(area[src0[j]], area[src1[j]]);
            area[dest[j]] = t0;
        } else if(opcode[j]==LD) {
            area[dest[j]] = t0;
            //printf("area total is : %f", area_total);
        } else if(opcode[j]==ST) {
            continue;
        }
    }
    return (int)area_total;
} 

#endif
