/***********************************************************************
 * Adaptive Simulated Annealing (ASA)
 * Lester Ingber <ingber@ingber.com>
 * Copyright (c) 1987-2013 Lester Ingber.  All Rights Reserved.
 * ASA-LICENSE file has the license that must be included with ASA code.
 ***********************************************************************/

/* $Id: asa_usr_cst.c,v 29.6 2013/10/19 21:30:59 ingber Exp ingber $ */

/* asa_usr_cst.c for Adaptive Simulated Annealing */

#include "asa_usr.h"
#include "area.h"
#include "opcode.h"
#include<time.h>
#if COST_FILE

#include<iostream>
#include<vector>
#define HOST(x,y,size) std::vector<y> hv_##x(size);
#define MAX_INSTRUCTIONS 128
#define MAX_INPUTS 128

typedef struct {
    int INSTRUCTIONS;
    int INPUTS;
    int OUTPUTS;
    int ST_ADDR;
} asm_stuff;

asm_stuff parse_asm(char* filename, std::vector<int> *opcode, std::vector<int> *src0, std::vector<int> *src1, std::vector<double> *in_lo, std::vector<double> *in_hi, std::vector<int> *dest) 
{
    FILE *fp;
    fp = fopen(filename,"r");
    if(!fp) {
        printf("%s does not exist\n",filename);
        exit(1);
    }
    int i=0;
    int ret;
    int loads=0, stores=0;
    asm_stuff stuff;
    while (!feof(fp)) {
        ret = fscanf(fp,"%d,%d,",&((*opcode)[i]),&((*dest)[i]));
        if(ret!=-1) {
            if((*opcode)[i]==LD) 
                loads++;
            if((*opcode)[i]==ST)
                stores++;
            if((*opcode)[i]==LD) {
                ret = fscanf(fp,"%lf,%lf;\n",&((*in_lo)[(*dest)[i]]), &((*in_hi)[(*dest)[i]]));
                if(ret!=-1) {
                    (*src0)[i]=-1;
                    (*src1)[i]=-1;
                }
            } else {
                ret = fscanf(fp,"%d,%d;\n",&((*src0)[i]),&((*src1)[i]));
                if(ret!=-1) {
                    if((*opcode)[i]==ST) {
                        stuff.ST_ADDR = (*src0)[i];
                    }
                }
            }
            i++;
        }
    }
    fclose(fp);
    stuff.INSTRUCTIONS=i;
    stuff.INPUTS=loads;
    stuff.OUTPUTS=stores;
    return stuff;
}

/* Note that this is a trimmed version of the ASA_TEST problem.
   A version of this cost_function with more documentation and hooks for
   various templates is in asa_usr.c. */

/* If you use this file to define your cost_function (the default),
   insert the body of your cost function just above the line
   "#if ASA_TEST" below.  (The default of ASA_TEST is FALSE.)

   If you read in information via the asa_opt file (the default),
   define *parameter_dimension and
   parameter_lower_bound[.], parameter_upper_bound[.], parameter_int_real[.]
   for each parameter at the bottom of asa_opt.

   The minimum you need to do here is to use
   x[0], ..., x[*parameter_dimension-1]
   for your parameters and to return the value of your cost function.  */

#if HAVE_ANSI
    double
cost_function (double *x,
        double *parameter_lower_bound,
        double *parameter_upper_bound,
        double *cost_tangents,
        double *cost_curvature,
        ALLOC_INT * parameter_dimension,
        int *parameter_int_real,
        int *cost_flag, int *exit_code, USER_DEFINES * USER_OPTIONS)
#else
    double
cost_function (x,
        parameter_lower_bound,
        parameter_upper_bound,
        cost_tangents,
        cost_curvature,
        parameter_dimension,
        parameter_int_real, cost_flag, exit_code, USER_OPTIONS)
    double *x;
    double *parameter_lower_bound;
    double *parameter_upper_bound;
    double *cost_tangents;
    double *cost_curvature;
    ALLOC_INT *parameter_dimension;
    int *parameter_int_real;
    int *cost_flag;
    int *exit_code;
    USER_DEFINES *USER_OPTIONS;
#endif
{

    /* *** Insert the body of your cost function here, or warnings
     * may occur if COST_FILE = TRUE & ASA_TEST != TRUE ***
     * Include ADAPTIVE_OPTIONS below if required */

    int ret = 0;
    char temp[100];
    int i;
    
    HOST(opcode,int, MAX_INSTRUCTIONS);
    HOST(src0,int, MAX_INSTRUCTIONS);
    HOST(src1,int, MAX_INSTRUCTIONS);
    HOST(dest,int, MAX_INSTRUCTIONS);
    HOST(in_lo,double, MAX_INPUTS);
    HOST(in_hi,double, MAX_INPUTS);

    sprintf(temp, "%s.asm", DESIGN);
    asm_stuff stuff = parse_asm(temp, &hv_opcode, &hv_src0, &hv_src1, &hv_in_lo, &hv_in_hi, &hv_dest);
    /*
       FILE *f = fopen("asa_generated_header.h", "w");
       if(f = NULL)
       {
       printf("Error opening file!\n"); 
       exit(1);
       }

       for(i=0; i < *parameter_dimension; i++)
       fprintf(f, "#define  VAR%d_BITS  %d\n", i, (int)x[i]);
       fclose(f);

       for(i=0; i< *parameter_dimension; i++)
       ret += (int)x[i];

*/

    int ix;
   // printf("\nSTEP1: Bitwidth Setup. ");
    for(ix=0; ix < *parameter_dimension; ix++)
    {
        sprintf(temp, "sed -i -e \'s/VAR%d_BITS .*/VAR%d_BITS %d/g\'  ./%s_vivado/asa_generated_header.h", ix+1, ix+1, (int)x[ix], DESIGN);
        system((char *)temp);
        sprintf(temp, "sed -i -e \'s/@fx%d = fixed<-.*,/@fx%d = fixed<-%d,/g\'  ./%s_vivado/%s.g", ix+1, ix+1, (int)x[ix], DESIGN, DESIGN);
        system((char *)temp);
    }
   // printf("    done ... \n");

   // printf("\nSTEP2: Error Bounding. ");
    sprintf(temp, "./%s_vivado/gappa.sh", DESIGN);
    FILE *constraint = popen(temp, "r");
    char lfix[50];
    char hfix[50];
    char ldbl[50];
    char hdbl[50];
    fgets(lfix,sizeof(lfix),constraint);
    fgets(hfix,sizeof(hfix),constraint);
    fgets(ldbl,sizeof(ldbl),constraint);
    fgets(hdbl,sizeof(hdbl),constraint);
    pclose(constraint);
   // printf("    done... \n");

    double low_fx_err, high_fx_err;
    double low_dbl_err, high_dbl_err;
    sscanf(lfix,"%lf", &low_fx_err);
    printf("fixed-point error lower bound is : %e\n", low_fx_err);
    sscanf(ldbl,"%lf", &low_dbl_err);
    printf("double error lower bound is : %e\n", low_dbl_err);
    sscanf(hfix,"%lf", &high_fx_err);
    printf("fixed-point error high bound is : %e\n", high_fx_err);
    sscanf(hdbl,"%lf", &high_dbl_err);
    printf("double error high bound is : %e\n", high_dbl_err);

    if(low_fx_err < low_dbl_err || high_fx_err > high_dbl_err) 
    {
        *cost_flag = FALSE;
        printf("*cost_flag = FALSE !\n");
    }
    else
        *cost_flag = TRUE;


    //printf("\nSTEP3: Resource Modeling. \n");
    if(*cost_flag == FALSE)
        ; //printf("cost_flag is false: invalid bitwidth combination...\n");
    else
    {
        // MODEL=0: analytic, MODEL=1: HLS 
        if(MODEL == 1)
        {
            printf("Current precision combination: \n");
            int j;
            for(j=0; j< *parameter_dimension; j++)
                printf("x[%d] = %d\n", j, (int)x[j]);
            sprintf(temp, "./%s_vivado/resource.sh", DESIGN);
            FILE *area = popen(temp,"r");
            char lut[20];
            fgets(lut,sizeof(lut),area);
            ret = atoi(lut);
            printf("The number of LUTs =  %d\n", ret);
            //for(ix=0;ix<*parameter_dimension;ix++)
            //    ret += (int)x[ix];
            //printf("LUTs + All Fractional Bits = %d \n", ret);
            pclose(area);
        }
        else if(MODEL == 0)
        {
            ret = peace_area(hv_opcode, hv_src0, hv_src1, hv_dest, stuff.INSTRUCTIONS, x);
            std::cout << "ret: " << ret << std::endl << std::endl;
        }
    }
    // record system time
    //time_t rawtime;
    //struct tm * timeinfo;

    //time ( &rawtime );
    //timeinfo = localtime ( &rawtime );
    //printf( "Current local time and date: %s\n", asctime (timeinfo) );  

    return ret;

#if ASA_TEST
#else
#if ADAPTIVE_OPTIONS
    adaptive_options (USER_OPTIONS);
#endif
#endif

#if ASA_TEST
    double q_n, d_i, s_i, t_i, z_i, c_r;
    int k_i;
    ALLOC_INT i, j;
    static LONG_INT funevals = 0;

#if ADAPTIVE_OPTIONS
    adaptive_options (USER_OPTIONS);
#endif

    s_i = 0.2;
    t_i = 0.05;
    c_r = 0.15;

    q_n = 0.0;
    for (i = 0; i < *parameter_dimension; ++i) {
        j = i % 4;
        switch (j) {
            case 0:
                d_i = 1.0;
                break;
            case 1:
                d_i = 1000.0;
                break;
            case 2:
                d_i = 10.0;
                break;
            default:
                d_i = 100.0;
        }
        if (x[i] > 0.0) {
            k_i = (int) (x[i] / s_i + 0.5);
        } else if (x[i] < 0.0) {
            k_i = (int) (x[i] / s_i - 0.5);
        } else {
            k_i = 0;
        }

        if (fabs (k_i * s_i - x[i]) < t_i) {
            if (k_i < 0) {
                z_i = k_i * s_i + t_i;
            } else if (k_i > 0) {
                z_i = k_i * s_i - t_i;
            } else {
                z_i = 0.0;
            }
            q_n += c_r * d_i * z_i * z_i;
        } else {
            q_n += d_i * x[i] * x[i];
        }
    }
    funevals = funevals + 1;

    *cost_flag = TRUE;

#if FALSE                       /* may set to TRUE if printf() is active */
#if TIME_CALC
    if ((PRINT_FREQUENCY > 0) && ((funevals % PRINT_FREQUENCY) == 0)) {
        printf ("funevals = %ld  ", funevals);
        print_time ("", stdout);
    }
#endif
#endif

#if ASA_FUZZY
    if (*cost_flag == TRUE
            && (USER_OPTIONS->Locate_Cost == 2 || USER_OPTIONS->Locate_Cost == 3
                || USER_OPTIONS->Locate_Cost == 4)) {
        FuzzyControl (USER_OPTIONS, x, q_n, *parameter_dimension);
    }
#endif /* ASA_FUZZY */

    return (q_n);
#endif /* ASA_TEST */
}
#endif /* COST_FILE */

