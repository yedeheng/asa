#!/bin/zsh
# this is for old version gappa 0.16
gappa /tmp/diode.g >& /tmp/diode_gappa.log
cat /tmp/diode_gappa.log | grep "_m.*_m" | grep -m 1 _fx | sed "s/.* in \[\(.*\)\, \(.*\)\]/\1/"
cat /tmp/diode_gappa.log | grep "_m.*_m" | grep -m 1 _fx | sed "s/.* in \[\(.*\)\, \(.*\)\]/\2/"
#echo "$low" > low_constraint.dat
#echo "$high" > high_constraint.dat
