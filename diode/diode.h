// ccautoesl autocompiled header file
// tdfc version 1.160
// Mon Oct 28 10:36:36 2013


#ifndef diode_H_
#define diode_H_
//#define SC_INCLUDE_FX
#include "ap_fixed.h"
//#include "systemc.h"

typedef ap_fixed<56,8> data_t1;
typedef ap_fixed<61,8> data_t2;
typedef ap_fixed<70,8> data_t3;
typedef ap_fixed<47,8> data_t4;
typedef ap_fixed<70,8> data_t5;
typedef ap_fixed<49,8> data_t6;
typedef ap_fixed<55,8> data_t7;
typedef ap_fixed<54,8> data_t8;
data_t5 exp_flopoco( data_t4 in); 
void diode(
	data_t1 cc_v,
	data_t8 *cc_i
);

#endif /*diode_H_*/

